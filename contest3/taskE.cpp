#include <algorithm>
#include <iostream>
#include <vector>

struct Edge {
  int u, v, w;
};

const int cInf = 100000;

std::vector<Edge> InputGraph(int n) {
  std::vector<Edge> g;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      int w;
      std::cin >> w;
      if (w != cInf) {
        g.push_back({i, j, w});
      }
    }
  }
  return g;
}

std::pair<std::vector<int>, std::vector<int>>
FordBellman(std::vector<Edge>& g, int n, int m, int& x) {
  std::vector<int> dist(n, 0);
  std::vector<int> par(n, -1);
  for (int i = 0; i < n; ++i) {
    x = -1;
    for (int j = 0; j < m; ++j) {
      if (dist[g[j].v] > dist[g[j].u] + g[j].w) {
        dist[g[j].v] = dist[g[j].u] + g[j].w;
        x = g[j].v;
        par[g[j].v] = g[j].u;
      }
    }
  }
  return {dist, par};
}

void PrintCycle(std::vector<int>& par, int x) {
  int y = x;
  for (int i = 0; i < static_cast<int>(par.size()); ++i) {
    y = par[y];
  }
  std::vector<int> path;
  for (int cur = y;; cur = par[cur]) {
    path.push_back(cur + 1);
    if (cur == y && path.size() > 1) {
      break;
    }
  }
  std::cout << path.size() << '\n';
  for (int i = static_cast<int>(path.size()) - 1; i >= 0; --i) {
    std::cout << path[i] << ' ';
  }
}

int main() {
  int n;
  std::cin >> n;
  auto g = InputGraph(n);
  int m = static_cast<int>(g.size());
  int x = 0;
  auto [dist, par] = FordBellman(g, n, m, x);
  if (x == -1) {
    std::cout << "NO\n";
    return 0;
  }
  std::cout << "YES\n";
  PrintCycle(par, x);
}
