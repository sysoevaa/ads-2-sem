#include <algorithm>
#include <iostream>
#include <vector>

void Dfs(int v, std::vector<int>& used, std::vector<std::vector<int>>& g) {
  used[v] = 1;
  for (int i : g[v]) {
    if (used[i] == 0) {
      Dfs(i, used, g);
    }
  }
}

void InputGraph(std::vector<std::vector<int>>& fg, int n, int m) {
  for (int i = 0; i < m; ++i) {
    int k;
    std::cin >> k;
    for (int j = 0; j < k; ++j) {
      int u;
      std::cin >> u;
      fg[i].push_back(u - 1 + m);
    }
  }
}

std::vector<std::vector<int>>
ReconstructGraph(std::vector<std::vector<int>>& fg, int n, int m,
                 std::vector<int>& in_match) {
  std::vector<std::vector<int>> g(n + m);
  for (int i = 0; i < m; ++i) {
    int u;
    std::cin >> u;
    u--;
    if (u == -1) {
      g[i] = fg[i];
      continue;
    }
    in_match[i] = 1;
    in_match[u + m] = 1;
    for (int j = 0; j < static_cast<int>(fg[i].size()); ++j) {
      if (fg[i][j] == u + m) {
        g[fg[i][j]].push_back(i);
      } else {
        g[i].push_back(fg[i][j]);
      }
    }
  }
  return g;
}

std::pair<std::vector<int>, std::vector<int>>
FindUnions(std::vector<std::vector<int>>& g, int n, int m,
           std::vector<int>& in_match) {
  std::vector<int> used(n + m, 0);
  for (int i = 0; i < m; ++i) {
    if (in_match[i] == 0 && used[i] == 0) {
      Dfs(i, used, g);
    }
  }
  std::vector<int> left;
  std::vector<int> right;
  for (int i = 0; i < m; ++i) {
    if (used[i] == 0) {
      left.push_back(i + 1);
    }
  }
  for (int i = m; i < n + m; ++i) {
    if (used[i] == 1) {
      right.push_back(i - m + 1);
    }
  }
  return {left, right};
}

void Print(std::vector<int>& left, std::vector<int>& right, int n, int m) {
  std::cout << left.size() + right.size() << '\n';
  std::cout << left.size() << ' ';
  for (auto i : left) {
    std::cout << i << ' ';
  }
  std::cout << '\n';
  std::cout << right.size() << ' ';
  for (auto i : right) {
    std::cout << i << ' ';
  }
}

signed main() {
  int n;
  int m;
  std::cin >> m >> n;
  std::vector<std::vector<int>> fg(n + m);
  InputGraph(fg, n, m);
  std::vector<int> in_match(n + m, 0);
  auto g = ReconstructGraph(fg, n, m, in_match);
  auto [left, right] = FindUnions(g, n, m, in_match);
  Print(left, right, n, m);
}
