#include <algorithm>
#include <iostream>
#include <vector>

class DSU {
  public:
  DSU(size_t size) {
    size_ = size;
    par_.resize(size);
    rank_.assign(size, 1);
    for (size_t i = 0; i < size_; ++i) {
      par_[i] = i;
    }
  }

  size_t Get(size_t u) {
    if (u == par_[u]) {
      return u;
    }
    return par_[u] = Get(par_[u]);
  }

  void Join(size_t u, size_t v) {
    u = Get(u);
    v = Get(v);
    if (u == v) {
      return;
    }
    if (rank_[v] > rank_[u]) {
      std::swap(u, v);
    }
    par_[v] = u;
    if (rank_[v] == rank_[u]) {
      rank_[u]++;
    }
  }

  private:
  std::vector<size_t> par_, rank_;
  size_t size_;
};

signed main() {
  int n;
  int m;
  std::cin >> n >> m;
  std::vector<std::pair<int, std::pair<int, int>>> e(m);
  for (int i = 0; i < m; ++i) {
    std::cin >> e[i].second.first >> e[i].second.second >> e[i].first;
  }
  DSU d(n + 1);
  std::sort(e.begin(), e.end());
  long long ans = 0;
  for (int i = 0; i < m; ++i) {
    if (d.Get(e[i].second.first) != d.Get(e[i].second.second)) {
      ans += e[i].first;
      d.Join(e[i].second.first, e[i].second.second);
    }
  }
  std::cout << ans;
}
