#include <algorithm>
#include <bitset>
#include <iostream>
#include <vector>

const long long kMod = 1e9 + 7;
const int kSize = 33000;

bool IsInRow(const std::bitset<kSize>& mask, unsigned int mask_size) {
  for (unsigned int i = 0; i < mask_size - 1; ++i) {
    if (mask[i] == mask[i + 1]) {
      return true;
    }
  }
  return false;
}

int CalcInRow(std::vector<std::vector<int>>& mat,
              const std::bitset<kSize>& mask, unsigned int size_n,
              unsigned int size_m) {
  auto inverted = ~mask;
  for (unsigned int j = 0; j < size_m; j++) {
    auto current = mask;
    if (j % 2 == 0) {
      current = mask;
    } else {
      current = inverted;
    }
    for (unsigned int i = 0; i < size_n; i++) {
      if (mat[i][j] == 1 && !current[i]) {
        return 0;
      }
      if (mat[i][j] == -1 and current[i]) {
        return 0;
      }
    }
  }
  return 1;
}

bool CountRow(long long& ans, std::vector<std::vector<int>>& mat,
              const std::bitset<kSize>& mask, int coloumn) {
  bool ok_mask = true;
  bool ok_inv = true;
  for (int i = 0; i < static_cast<int>(mat.size()); i++) {
    if (mat[i][coloumn] == 1 && !mask[i]) {
      ok_mask = false;
    }
    if (mat[i][coloumn] == -1 && mask[i]) {
      ok_mask = false;
    }
    if (mat[i][coloumn] == 1 && mask[i]) {
      ok_inv = false;
    }
    if (mat[i][coloumn] == -1 && !mask[i]) {
      ok_inv = false;
    }
  }
  if (!ok_mask && !ok_inv) {
    return false;
  }
  if (ok_mask && ok_inv) {
    ans *= 2;
    ans %= kMod;
  }
  return true;
}

long long CalcNotInRow(std::vector<std::vector<int>>& mat,
                       const std::bitset<kSize>& mask, unsigned int size_n,
                       unsigned int size_m) {
  for (unsigned int i = 0; i < size_n; i++) {
    if (mat[i][0] == 1 && !mask[i]) {
      return 0;
    }
    if (mat[i][0] == -1 && mask[i]) {
      return 0;
    }
  }
  long long ans = 1;
  for (unsigned int j = 1; j < size_m; j++) {
    bool flag = CountRow(ans, mat, mask, j);
    if (!flag) {
      return 0;
    }
  }
  return ans;
}

signed main() {
  unsigned int size_n;
  unsigned int size_m;
  std::cin >> size_n >> size_m;
  std::vector<std::vector<int>> mat(size_n, std::vector<int>(size_m));
  for (unsigned int i = 0; i < size_n; ++i) {
    for (unsigned int j = 0; j < size_m; ++j) {
      char sym;
      std::cin >> sym;
      if (sym == '.') {
        mat[i][j] = 0;
      } else if (sym == '+') {
        mat[i][j] = 1;
      } else {
        mat[i][j] = -1;
      }
    }
  }
  long long ans = 0;
  for (unsigned int mask = 0; mask < (1 << size_n); ++mask) {
    if (IsInRow(mask, size_n)) {
      ans += CalcInRow(mat, mask, size_n, size_m);
      if (ans >= kMod) {
        ans %= kMod;
      }
    } else {
      ans += CalcNotInRow(mat, mask, size_n, size_m);
      ans %= kMod;
    }
  }
  std::cout << ans;
}
