#include <algorithm>
#include <iostream>
#include <vector>

const int kInf = 1e9;

int main() {
  int len;
  std::cin >> len;
  std::vector<int> arr(len);
  for (int i = 0; i < len; ++i) {
    std::cin >> arr[i];
  }
  std::vector<int> dp(len + 1, -kInf);
  std::vector<int> prev(len, -1);
  dp[0] = kInf;
  int max_len = 0;
  for (int i = 0; i < len; ++i) {
    int left = 0;
    int right = len;
    while (left < right) {
      int mid = left + (right - left) / 2;
      if (dp[mid] >= arr[i]) {
        left = mid + 1;
      } else {
        right = mid;
      }
    }
    dp[left] = arr[i];
    prev[i] = left - 1;
    max_len = std::max(max_len, left);
  }

  std::vector<int> result;
  int current = max_len - 1;
  for (int i = len - 1; i >= 0; --i) {
    if (prev[i] == current) {
      result.push_back(i + 1);
      --current;
    }
  }
  reverse(result.begin(), result.end());
  std::cout << result.size() << '\n';
  for (int num : result) {
    std::cout << num << " ";
  }
}

